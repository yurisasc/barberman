import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:barberman/src/resources/menu_repository.dart';
import 'package:barberman/src/model/menu.dart';
import 'package:barberman/src/model/user.dart';
import 'package:barberman/src/resources/user_repository.dart';
import 'package:barberman/src/model/stamp.dart';

class Transaction extends StatefulWidget {
  @override
  _TransactionState createState() => new _TransactionState();
}

class _TransactionState extends State<Transaction>{
  static const int PRICE_FREE = -1;

  final _phoneNumberController = TextEditingController();
  bool _isComposing = false;
  bool _hasSearched = false;
  bool _found = false;

  User _selectedUser;
  Menu _selectedMenu;
  List<bool> _isFreeCut;
  int _price = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          _buildPhoneInput(context),
          _hasSearched
              ? (_found ? _buildForm(context) : _notFound())
              : _hasNotSearched(),
        ],
      ),
    );
  }

  Widget _buildPhoneInput(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 48.0, horizontal: 32.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: TextField(
              onChanged: (String text) {
                setState(() {
                  _isComposing = text.length > 0;
                });
              },
              controller: _phoneNumberController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                filled: true,
                hintText: 'Phone number',
                fillColor: theme.scaffoldBackgroundColor,
              ),
            ),
          ),
          SizedBox(width: 16.0),
          MaterialButton(
            colorBrightness: Brightness.dark,
            disabledColor: theme.scaffoldBackgroundColor,
            onPressed: _isComposing
                ? () => _handleSubmitted(_phoneNumberController.text)
                : null,
            child: Icon(Icons.forward),
            minWidth: 45.0,
            height: 45.0,
            color: theme.accentColor,
          )
        ]
      ),
      decoration: BoxDecoration(
        color: theme.cardColor,
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(0xFF, 0x9E, 0x9E, 0x9E),
            offset: Offset(0.0, 3.0),
            blurRadius: 3.0
          )
        ]
      ),
    );
  }

  void _handleSubmitted(String text) {
    String _phone = _phoneNumberController.text;
    _phoneNumberController.clear();
    // close keyboard
    FocusScope.of(context).requestFocus(new FocusNode());

    List<User> users = UserRepository.loadUser();
    setState(() {
      _isComposing = false;
      _hasSearched = true;
      _selectedMenu = null;
      _price = 0;
      _found = users
          .map((user) {
            if(user.phone_number == _phone){
              _selectedUser = user;
              _isFreeCut = Stamp.isFreeCut(_selectedUser.stamps);
              return true;
            } else {
              return false;
            }})
          .toList().contains(true);
    });

  }

  Widget _buildForm(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    List<Menu> menus = MenuRepository.loadMenu();
    final NumberFormat formatter = NumberFormat.simpleCurrency(
        locale: "id_ID", name: "IDR");

    return Flexible(
      child: ListView(
//        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(16.0, 24.0, 16.0, 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("TRANSACTION FORM", style: theme.accentTextTheme.body2),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
                vertical: 24.0, horizontal: 48.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(_selectedUser.name, style: theme.textTheme.title),
                    SizedBox(width: 8.0),
                    _isFreeCut.contains(true)
                        ? Icon(Icons.stars, color: theme.accentColor)
                        : SizedBox(),
                  ],
                ),
                SizedBox(height: 18.0),
                Text("Service:", style: theme.accentTextTheme.body1),
                SizedBox(height: 8.0),
                DropdownButton<Menu>(
                  isExpanded: true,
                  hint: Text("Choose service"),
                  value: _selectedMenu,
                  onChanged: (Menu newValue) {
                    setState(() {
                      _selectedMenu = newValue;
                      _price = newValue.price;
                    });
                  },
                  items: menus.map((menu) {
                    return new DropdownMenuItem<Menu>(
                      value: menu,
                      child: Text(menu.name),
                    );
                  }).toList(),
                ),
                SizedBox(height: 10.0),
                Text("Price:", style: theme.accentTextTheme.body1),
                SizedBox(height: 16.0),
                Text(formatter.format(_price), style: theme.textTheme.body1),
                SizedBox(height: 16.0),
                Text("Stamps:", style: theme.accentTextTheme.body1),
                SizedBox(height: 16.0),
                Stamp.createStamp(context, _selectedUser.stamps),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _hasNotSearched() {
    final ThemeData theme = Theme.of(context);
    return Flexible(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
              "Please input the phone number",
              style: TextStyle(
                color: theme.hintColor,
                fontSize: 16.0,)
          )
        ],
      ),
    );
  }

  Widget _notFound() {
    final ThemeData theme = Theme.of(context);
    return Flexible(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.cancel,
            size: 35,
            color: theme.errorColor,
          ),
          Text(
              "Member does not exist. Please register.",
              style: TextStyle(
                color: theme.errorColor,
                fontSize: 16.0,)
          )
        ],
      ),
    );
  }
}
import 'package:flutter/material.dart';

final ThemeData defaultTheme = ThemeData(
  // Define the default Brightness and Colors
  brightness: Brightness.light,
  primaryColor: Color.fromARGB(0xFF, 0x21, 0x21, 0x21),
  accentColor: Color.fromARGB(0xFF, 0xFF, 0x47, 0x3A),
  cardColor: Color.fromARGB(0xFF, 0x61, 0x61, 0x61),
  scaffoldBackgroundColor: Colors.grey[200],
  hintColor: Color.fromARGB(0xFF, 0x75, 0x75, 0x75),

  textTheme: TextTheme(
    headline: TextStyle(
      color: Colors.black,
      fontSize: 72.0,
      fontWeight: FontWeight.bold,
    ),
    title: TextStyle(
      color: Colors.black,
      fontSize: 22.0,
    ),
    body1: TextStyle(
      color: Colors.black,
      fontSize: 16.0,
    ),
    subhead: TextStyle(
      color: Colors.black,
    ),

  ),

  accentTextTheme: TextTheme(
    headline: TextStyle(
        color: Color.fromARGB(0xFF, 0xFF, 0x47, 0x3A),
        fontSize: 72.0,
        fontWeight: FontWeight.bold,
        fontFamily: 'Montserrat'
    ),
    title: TextStyle(
        color: Color.fromARGB(0xFF, 0xFF, 0x47, 0x3A),
        fontSize: 36.0,
        fontStyle: FontStyle.italic,
        fontFamily: 'Montserrat'
    ),
    body1: TextStyle(
      color: Color.fromARGB(0xFF, 0xFF, 0x47, 0x3A),
      fontSize: 14.0,
      fontFamily: 'Montserrat',
    ),
    body2: TextStyle(
      color: Color.fromARGB(0xFF, 0xFF, 0x47, 0x3A),
      fontSize: 16.0,
      fontFamily: 'Montserrat',
    ),
  ),

);

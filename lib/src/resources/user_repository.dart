import 'package:barberman/src/model/user.dart';

class UserRepository {
  static const users = <User> [
    User(
      phone_number: "081235552635",
      name: "Yuris Aryansyah",
      stamps: 2,
    ),
    User(
      phone_number: "087886418952",
      name: "Rheza Prathama",
      stamps: 4,
    ),
    User(
      phone_number: "081234567890",
      name: "Ifan Widiarso",
      stamps: 4,
    ),
    User(
      phone_number: "080987654321",
      name: "Bhisma Hutama",
      stamps: 6,
    ),
  ];

  static List<User> loadUser() {
    return users;
  }

  static User byPhoneNumber(String phoneNumber) {
    User result;
    users.forEach((user) {
      if (user.phone_number == phoneNumber) {
        result = user;
      }
    });
    return result;
  }
}
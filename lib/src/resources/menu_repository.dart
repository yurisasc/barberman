import 'package:barberman/src/model/menu.dart';

class MenuRepository {
  static List<Menu> loadMenu() {
    const allMenu = <Menu> [
      Menu(
        id: 0,
        name: 'Classic',
        price: 25000,
      ),
      Menu(
        id: 1,
        name: 'Exclusive',
        price: 45000,
      )
    ];
    return allMenu;
  }
}
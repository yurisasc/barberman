import 'package:flutter/material.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:barberman/src/ui/analytics_widget.dart';
import 'package:barberman/src/ui/menu_widget.dart';
import 'package:barberman/src/resources/theme.dart';
import 'package:barberman/src/ui/transaction_widget.dart';

class App extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Man &\' Care',
      theme: defaultTheme,
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 1;
  final List<Widget> _children = [
    Transaction(),
    Transaction(),
    Transaction(),
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
            'Man &\' Care',
            style: TextStyle(
                color: Theme.of(context).scaffoldBackgroundColor,
                fontSize:24.0,
                fontFamily: 'Oleo Script Swash Caps'
            )
        ),
      ),
      body: SafeArea(
        child: _children[_currentIndex],
      ),
      bottomNavigationBar: _buildBottomNav(context),
    );
  }

  Widget _buildBottomNav(BuildContext context) {
    return new Theme(
      data: Theme.of(context).copyWith(
          canvasColor: const Color(0xFF202124),
          primaryColor: Theme.of(context).accentColor,
          textTheme: Theme
              .of(context)
              .textTheme
              .copyWith(caption: new TextStyle(color: Colors.white))
      ),
      child: new BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.list),
            title: new Text("Menu"),
          ),
          BottomNavigationBarItem(
            icon: new Icon(OMIcons.addBox),
            title: new Text("Transaction"),
          ),
          BottomNavigationBarItem(
            icon: new Icon(OMIcons.barChart),
            title: new Text("Analytics"),
          )
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
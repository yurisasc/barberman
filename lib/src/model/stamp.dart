import 'package:flutter/material.dart';

class Stamp {
  static const int STAMP_SIZE = 10;
  static const int FREE_CUT_REG = 5;
  static const int FREE_CUT_EXC = 10;
  static const List<String> FREE_CUT_TEXT = [
    "FREE CLASSIC",
    "FREE EXCLUSIVE"
  ];

  static Widget createStamp(BuildContext context, int userStamp) {
    List<Widget> icons = _buildIcons(userStamp);
    List<bool> freeCut = isFreeCut(userStamp);
    ThemeData theme = Theme.of(context);

    List<Widget> firstRow = icons.sublist(0, 5);
    List<Widget> secondRow = icons.sublist(5, 10);

    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: _buildRow(firstRow, freeCut[0], theme, FREE_CUT_TEXT[0]),
          ),
          Row(
            children: _buildRow(secondRow, freeCut[1], theme, FREE_CUT_TEXT[1]),
          )
        ],
      ),
    );
  }

  static List<Widget> _buildIcons(int userStamp) {
    return List.generate(
        STAMP_SIZE,
            (int i) => (i < userStamp + 1)
            ? (i+1 == FREE_CUT_REG || i+1 == FREE_CUT_EXC
              ? Icon(Icons.stars, size: 25, color: Colors.deepOrangeAccent)
              : Icon(Icons.radio_button_checked, size: 25))
            : Icon(Icons.radio_button_unchecked, size: 25)
    );
  }

  static List<bool> isFreeCut(int userStamp) {
    List<bool> freeCut = new List(2);

    userStamp + 1 == FREE_CUT_REG ? freeCut[0] = true : freeCut[0] = false;
    userStamp + 1 == FREE_CUT_EXC ? freeCut[1] = true : freeCut[1] = false;

    return freeCut;
  }

  static List<Widget> _buildRow
      (List<Widget> row, bool isFreeCut, ThemeData theme, String text) {

    row.add(new SizedBox(width: 10.0));
    row.add(new Text(
      text,
      style: TextStyle(
          fontSize: 12,
          color: isFreeCut ? theme.accentColor : theme.hintColor
      ),
    ));

    return row;
  }
}
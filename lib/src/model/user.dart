import 'package:flutter/foundation.dart';

class User {
  const User({
    @required this.phone_number,
    @required this.name,
    @required this.stamps,
  })  : assert (phone_number != null),
        assert (name != null),
        assert (stamps != null);

  final String phone_number;
  final String name;
  final int stamps;

  @override
  String toString() => "$name (phone=$phone_number)";
}
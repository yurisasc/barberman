import 'package:flutter/foundation.dart';

class Menu {
  const Menu({
    @required this.id,
    @required this.name,
    @required this.price
  })  : assert (id != null),
        assert (name != null),
        assert (price != null);

  final int id;
  final String name;
  final int price;

  @override
  String toString() => "$name (price=$price)";
}